package service

import (
	"demo-gateway/pb"
	"fmt"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type serviceClient struct {
	Student pb.StudentClient
}

func NewServiceClient() *serviceClient {
	clientConn, err := grpc.Dial(":8080", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		fmt.Println("Failed to connect to Designer service:", err)
	}

	return &serviceClient{
		Student: pb.NewStudentClient(clientConn),
	}

}

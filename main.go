package main

import (
	"demo-gateway/pb"
	"demo-gateway/service"
	"github.com/labstack/echo/v4"
	"net/http"
)

func main() {
	e := echo.New()
	regisService := service.NewServiceClient()
	e.GET("/", func(c echo.Context) error {
		res, err := regisService.Student.AddStudent(c.Request().Context(), &pb.StudentRequest{
			Name: "test",
			Age:  111,
		})
		if err != nil {
			return c.JSON(http.StatusInternalServerError, "ไม่พร้อมให้บริการ")
		}
		return c.JSON(http.StatusOK, res)
	})
	e.Logger.Fatal(e.Start(":1323"))
}
